defmodule Bashboard.DashTest do
  use ExUnit.Case
  use Plug.Test
  import Bashboard.MockData

  @opts Bashboard.Endpoint.init([])

  setup :db_connection

  describe "dashboard with all mocks." do
    setup :mock_all

    test "No chartify options." do
      conn =
        conn(:get, "/myuser/mydash")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      body = conn.resp_body |> Poison.decode!()

      assert %{
               "data" => [
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 3,
                     "datetime" => "\"2020-01-01T13:14:15Z\"",
                     "price" => 50
                   },
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               }
             } == body["widget-maps"]

      assert %{
               "data" => [%{"body" => 60, "datetime" => "2020-01-01T00:00:00Z"}],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               }
             } == body["widget-numbers"]

      assert %{
               "data" => [%{"body" => 1, "datetime" => "2020-01-01T00:00:00Z"}],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers-many"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               }
             } == body["widget-numbers-many"]

      assert %{
               "data" => [%{"body" => "foo", "datetime" => "2020-01-01T00:00:00Z"}],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-strings"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               }
             } == body["widget-strings"]
    end
  end
end
