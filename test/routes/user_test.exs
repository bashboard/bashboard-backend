defmodule Bashboard.UserTest do
  use ExUnit.Case
  use Plug.Test
  import Bashboard.MockData

  @opts Bashboard.Endpoint.init([])

  setup :db_connection

  test "User gets reserved" do
    conn(:post, "/myuser/res", Poison.encode!(%{email: "my@email.com"}))
    |> put_req_header("content-type", "application/json")
    |> Bashboard.Endpoint.call(@opts)

    conn =
      conn(:get, "/myuser/res")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert true = conn.resp_body |> Poison.decode!()
  end
end
