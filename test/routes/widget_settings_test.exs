defmodule Bashboard.WidgetSettingsTest do
  use ExUnit.Case
  use Plug.Test
  import Bashboard.MockData

  @opts Bashboard.Endpoint.init([])

  setup [:db_connection, :mock_widget_maps]

  test "Get widget with all possible settings", %{namespace: namespace} do
    conn =
      conn(:get, namespace <> "?all_confs=true")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    body = conn.resp_body |> Poison.decode!()

    assert length(body) == 14

    assert %{
             "data" => [
               %{
                 "body" => %{
                   "action" => "sell",
                   "amount" => 3,
                   "datetime" => "\"2020-01-01T13:14:15Z\"",
                   "price" => 50
                 },
                 "datetime" => "2020-01-01T00:00:00Z"
               }
             ],
             "meta" => %{
               "namespace" => ["myuser", "mydash", "widget-maps"],
               "timeseries" => "last",
               "transform" => nil
             }
           } == body |> List.first()
  end

  test "Save settings", %{namespace: namespace} do
    save_settings = %{
      "timeseries" => "minute",
      "transform" => "sum_map"
    }

    conn =
      conn(:post, namespace <> "/settings", Poison.encode!(save_settings))
      |> put_req_header("content-type", "application/json")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "success"

    conn =
      conn(:get, namespace <> "/settings")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert save_settings == conn.resp_body |> Poison.decode!() |> Map.get("body")
  end
end
