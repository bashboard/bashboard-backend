defmodule Bashboard.WidgetTest do
  use ExUnit.Case
  use Plug.Test
  import Bashboard.MockData

  @opts Bashboard.Endpoint.init([])

  setup :db_connection

  test "save widget data" do
    conn =
      conn(:post, "/myuser/mydash/basic", Poison.encode!(10))
      |> put_req_header("content-type", "application/json")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "success"
  end

  test "load empty widget data" do
    conn =
      conn(:get, "/myuser/mydash/basic")
      |> put_req_header("content-type", "application/json")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert %{
             "meta" => %{
               "namespace" => ["myuser", "mydash", "basic"],
               "no_settings" => true,
               "timeseries" => "last",
               "transform" => nil
             },
             "data" => []
           } === conn.resp_body |> Poison.decode!()
  end

  test "End to end. json." do
    for no <- [10, 11, 12, 13, 14, 15] do
      conn(:post, "/myuser/mydash/basic", Poison.encode!(no))
      |> put_req_header("content-type", "application/json")
      |> Bashboard.Endpoint.call(@opts)
    end

    Bashboard.ContinuousAggregator.all_once()

    conn =
      conn(:get, "/myuser/mydash/basic")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert %{
             "data" => _,
             "meta" => %{
               "namespace" => ["myuser", "mydash", "basic"],
               "no_settings" => true,
               "timeseries" => "last",
               "transform" => nil
             }
           } = conn.resp_body |> Poison.decode!()
  end

  test "End to end. Empty body in POST request." do
    for _no <- [10, 11, 12, 13, 14, 15] do
      conn(:post, "/myuser/mydash/basic")
      |> put_req_header("content-type", "application/json")
      |> Bashboard.Endpoint.call(@opts)
    end

    Bashboard.ContinuousAggregator.all_once()

    conn =
      conn(:get, "/myuser/mydash/basic")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert %{
             "data" => [%{"body" => nil, "datetime" => _}],
             "meta" => %{
               "namespace" => ["myuser", "mydash", "basic"],
               "no_settings" => true,
               "timeseries" => "last",
               "transform" => nil
             }
           } = conn.resp_body |> Poison.decode!()
  end

  describe "End to end with special headers." do
    test "No headers." do
      for no <- [10, 11, 12, 13, 14, 15] do
        conn(:post, "/myuser/mydash/basic", Poison.encode!(no))
        |> Bashboard.Endpoint.call(@opts)
      end

      Bashboard.ContinuousAggregator.all_once()

      conn =
        conn(:get, "/myuser/mydash/basic")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [%{"body" => _, "datetime" => _}],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "basic"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               }
             } = conn.resp_body |> Poison.decode!()
    end

    test "Form headers." do
      for no <- [10, 11, 12, 13, 14, 15] do
        conn(:post, "/myuser/mydash/basic", Poison.encode!(no))
        |> put_req_header("content-type", "application/x-www-form-urlencoded")
        |> Bashboard.Endpoint.call(@opts)
      end

      Bashboard.ContinuousAggregator.all_once()

      conn =
        conn(:get, "/myuser/mydash/basic")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [%{"body" => _, "datetime" => _}],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "basic"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               }
             } = conn.resp_body |> Poison.decode!()
    end

    test "Fetch default headers." do
      for no <- [10, 11, 12, 13, 14, 15] do
        conn(:post, "/myuser/mydash/basic", Poison.encode!(no))
        |> put_req_header("content-type", "text/plain")
        |> Bashboard.Endpoint.call(@opts)
      end

      Bashboard.ContinuousAggregator.all_once()

      conn =
        conn(:get, "/myuser/mydash/basic")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [%{"body" => _, "datetime" => _}],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "basic"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               }
             } = conn.resp_body |> Poison.decode!()
    end
  end

  test "End to end with whitespaces" do
    widget_url = "/my user/my dash/white space every where"

    for no <- [10, 11, 12, 13, 14, 15] do
      conn(:post, widget_url, Poison.encode!(no))
      |> put_req_header("content-type", "application/json")
      |> Bashboard.Endpoint.call(@opts)
    end

    Bashboard.ContinuousAggregator.all_once()

    conn =
      conn(:get, widget_url |> String.replace(" ", "%20", global: true))
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    # Only test where vague values allowed, because we use the router
    assert %{
             "data" => _,
             "meta" => %{
               "namespace" => ["my user", "my dash", "white space every where"],
               "no_settings" => true,
               "timeseries" => "last",
               "transform" => nil
             }
           } = conn.resp_body |> Poison.decode!()
  end

  describe "Numbers widget." do
    setup :mock_widget_numbers

    test "No chartify options.", %{namespace: namespace} do
      conn =
        conn(:get, namespace)
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               },
               "data" => [%{"body" => 60, "datetime" => "2020-01-01T00:00:00Z"}]
             } == conn.resp_body |> Poison.decode!()
    end

    test "{minute, nil}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?timeseries=minute")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{"body" => 1, "datetime" => "2019-12-31T23:58:00Z"},
                 %{"body" => 2, "datetime" => "2019-12-31T23:59:00Z"},
                 %{"body" => 1, "datetime" => "2020-01-01T00:00:00Z"}
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers"],
                 "transform" => "count_messages",
                 "timeseries" => "minute"
               }
             } == conn.resp_body |> Poison.decode!()
    end

    test "{minute, sum_numbers}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?timeseries=minute&transform=sum_numbers")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{"body" => 4.0, "datetime" => "2019-12-31T23:58:00.000000"},
                 %{"body" => 13.6, "datetime" => "2019-12-31T23:59:00.000000"},
                 %{"body" => 60.0, "datetime" => "2020-01-01T00:00:00.000000"}
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers"],
                 "timeseries" => "minute",
                 "transform" => "sum_numbers"
               }
             } == conn.resp_body |> Poison.decode!()
    end

    test "{minute, avg_numbers}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?timeseries=minute&transform=avg_numbers")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{"body" => 4.0, "datetime" => "2019-12-31T23:58:00.000000"},
                 %{"body" => 6.8, "datetime" => "2019-12-31T23:59:00.000000"},
                 %{"body" => 60.0, "datetime" => "2020-01-01T00:00:00.000000"}
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers"],
                 "timeseries" => "minute",
                 "transform" => "avg_numbers"
               }
             } == conn.resp_body |> Poison.decode!()
    end

    test "{minute, avg_numbers, datetime}", %{namespace: namespace} do
      conn =
        conn(
          :get,
          namespace <>
            "?timeseries=minute&transform=avg_numbers&datetime=2020-01-01T00%3A00%3A00Z"
        )
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{"body" => 60.0, "datetime" => "2020-01-01T00:00:00.000000"}
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers"],
                 "timeseries" => "minute",
                 "transform" => "avg_numbers"
               }
             } == conn.resp_body |> Poison.decode!()
    end
  end

  describe "Numbers with time gaps." do
    setup :mock_widget_numbers_gaps

    test "{minute, avg}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?ts=m&tr=avg")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{"body" => 7.0, "datetime" => "2019-12-31T23:45:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:46:00.000000"},
                 %{"body" => 6.0, "datetime" => "2019-12-31T23:47:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:48:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:49:00.000000"},
                 %{"body" => 5.0, "datetime" => "2019-12-31T23:50:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:51:00.000000"},
                 %{"body" => 4.0, "datetime" => "2019-12-31T23:52:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:53:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:54:00.000000"},
                 %{"body" => 3.0, "datetime" => "2019-12-31T23:55:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:56:00.000000"},
                 %{"body" => 2.0, "datetime" => "2019-12-31T23:57:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:58:00.000000"},
                 %{"body" => nil, "datetime" => "2019-12-31T23:59:00.000000"},
                 %{"body" => 1.0, "datetime" => "2020-01-01T00:00:00.000000"}
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers-gaps"],
                 "timeseries" => "minute",
                 "transform" => "avg_numbers"
               }
             } == conn.resp_body |> Poison.decode!()
    end
  end

  describe "Numbers (many) widget." do
    setup :mock_widget_numbers_many

    test "No chartify options.", %{namespace: namespace} do
      conn =
        conn(:get, namespace)
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-numbers-many"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               },
               "data" => [%{"body" => 1, "datetime" => "2020-01-01T00:00:00Z"}]
             } == conn.resp_body |> Poison.decode!()
    end
  end

  describe "Strings widget." do
    setup :mock_widget_strings

    test "No chartify options.", %{namespace: namespace} do
      conn =
        conn(:get, namespace)
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-strings"],
                 "timeseries" => "last",
                 "no_settings" => true,
                 "transform" => nil
               },
               "data" => [%{"body" => "foo", "datetime" => "2020-01-01T00:00:00Z"}]
             } == conn.resp_body |> Poison.decode!()
    end

    test "{nil, categorize_strings}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?trans=cat")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{"body" => %{"foo" => 1}, "datetime" => "2019-12-31T23:57:00Z"},
                 %{"body" => %{"bar" => 1, "foo" => 1}, "datetime" => "2019-12-31T23:58:00Z"},
                 %{"body" => %{"bar" => 1, "baz" => 1}, "datetime" => "2019-12-31T23:59:00Z"},
                 %{"body" => %{"foo" => 1}, "datetime" => "2020-01-01T00:00:00Z"}
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-strings"],
                 "timeseries" => "minute",
                 "transform" => "categorize_strings"
               }
             } == conn.resp_body |> Poison.decode!()
    end

    test "{hour, categorize_strings}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?ts=h&trans=cat")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{
                   "body" => %{"foo" => 2, "bar" => 2, "baz" => 1},
                   "datetime" => "2019-12-31T23:00:00Z"
                 },
                 %{"body" => %{"foo" => 1}, "datetime" => "2020-01-01T00:00:00Z"}
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-strings"],
                 "transform" => "categorize_strings",
                 "timeseries" => "hour"
               }
             } == conn.resp_body |> Poison.decode!()
    end
  end

  describe "Map widget." do
    setup :mock_widget_maps

    test "No chartify options (+ fake param).", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?chart=line")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "no_settings" => true,
                 "timeseries" => "last",
                 "transform" => nil
               },
               "data" => [
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 3,
                     "datetime" => "\"2020-01-01T13:14:15Z\"",
                     "price" => 50
                   },
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ]
             } == conn.resp_body |> Poison.decode!()
    end

    test "{nil, avg_map}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?transform=avg_map")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "data" => [
                 %{"body" => %{"price" => 43}, "datetime" => "2019-12-31T23:57:00Z"},
                 %{
                   "body" => %{"amount" => 7.5, "price" => 50},
                   "datetime" => "2019-12-31T23:58:00Z"
                 },
                 %{
                   "body" => %{"amount" => 6.5, "price" => 48.5},
                   "datetime" => "2019-12-31T23:59:00Z"
                 },
                 %{
                   "body" => %{"amount" => 3, "price" => 50},
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "minute",
                 "transform" => "avg_map"
               }
             } == conn.resp_body |> Poison.decode!()
    end

    test "{minute, count_messages}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?ts=m&transform=count")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "minute",
                 "transform" => "count_messages"
               },
               "data" => [
                 %{"body" => 1, "datetime" => "2019-12-31T23:57:00Z"},
                 %{"body" => 2, "datetime" => "2019-12-31T23:58:00Z"},
                 %{"body" => 2, "datetime" => "2019-12-31T23:59:00Z"},
                 %{"body" => 1, "datetime" => "2020-01-01T00:00:00Z"}
               ]
             } == conn.resp_body |> Poison.decode!()
    end

    test "{nil, sum_map}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?transform=sum_map")
        |> Bashboard.Endpoint.call(@opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "minute",
                 "transform" => "sum_map"
               },
               "data" => [
                 %{"body" => %{"price" => 43}, "datetime" => "2019-12-31T23:57:00Z"},
                 %{
                   "body" => %{"amount" => 15, "price" => 100},
                   "datetime" => "2019-12-31T23:58:00Z"
                 },
                 %{
                   "body" => %{"amount" => 13, "price" => 97},
                   "datetime" => "2019-12-31T23:59:00Z"
                 },
                 %{
                   "body" => %{"amount" => 3, "price" => 50},
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ]
             } == conn.resp_body |> Poison.decode!()
    end

    test "{hour, sum_map}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?timeseries=hour&transform=sum_map")
        |> Bashboard.Endpoint.call(@opts)

      assert %{
               "data" => [
                 %{
                   "body" => %{"amount" => 28, "price" => 240},
                   "datetime" => "2019-12-31T23:00:00Z"
                 },
                 %{
                   "body" => %{"amount" => 3, "price" => 50},
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ],
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "hour",
                 "transform" => "sum_map"
               }
             } == conn.resp_body |> Poison.decode!()
    end

    test "{raw}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?raw=true")
        |> Bashboard.Endpoint.call(@opts)

      results = conn.resp_body |> Poison.decode!()

      assert 6 == length(results["data"])

      assert %{
               "body" => %{
                 "action" => "sell",
                 "amount" => 3,
                 "datetime" => "\"2020-01-01T13:14:15Z\"",
                 "price" => 50
               },
               "datetime" => "2020-01-01T00:00:00Z"
             } = results["data"] |> Enum.at(5)
    end

    test "{last}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?timeseries=last")
        |> Bashboard.Endpoint.call(@opts)

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "last",
                 "transform" => nil
               },
               "data" => [
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 3,
                     "datetime" => "\"2020-01-01T13:14:15Z\"",
                     "price" => 50
                   },
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ]
             } == conn.resp_body |> Poison.decode!()
    end

    test "{last, fake}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?timeseries=last&transform=whatever")
        |> Bashboard.Endpoint.call(@opts)

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "last",
                 "transform" => nil
               },
               "data" => [
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 3,
                     "datetime" => "\"2020-01-01T13:14:15Z\"",
                     "price" => 50
                   },
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ]
             } == conn.resp_body |> Poison.decode!()
    end

    test "{series}", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?timeseries=series")
        |> Bashboard.Endpoint.call(@opts)

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "series",
                 "transform" => nil
               },
               "data" => [
                 %{
                   "body" => %{"action" => "buy", "price" => 43},
                   "datetime" => "2019-12-31T23:57:30Z"
                 },
                 %{
                   "body" => %{"action" => "sell", "amount" => 11, "price" => 49},
                   "datetime" => "2019-12-31T23:58:00Z"
                 },
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 4,
                     "datetime" => "\"2020-01-20T15:19:20Z\"",
                     "price" => 51
                   },
                   "datetime" => "2019-12-31T23:58:30Z"
                 },
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 5,
                     "datetime" => "\"2020-01-01T14:16:20Z\"",
                     "price" => 49
                   },
                   "datetime" => "2019-12-31T23:59:00Z"
                 },
                 %{
                   "body" => %{
                     "action" => "buy",
                     "amount" => 8,
                     "datetime" => "\"2020-01-01T13:15:20Z\"",
                     "price" => 48
                   },
                   "datetime" => "2019-12-31T23:59:30Z"
                 },
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 3,
                     "datetime" => "\"2020-01-01T13:14:15Z\"",
                     "price" => 50
                   },
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ]
             } == conn.resp_body |> Poison.decode!()
    end

    test "All possible configurations.", %{namespace: namespace} do
      conn =
        conn(:get, namespace <> "?all_confs=true")
        |> Bashboard.Endpoint.call(@opts)

      result = conn.resp_body |> Poison.decode!()

      assert %{
               "meta" => %{
                 "namespace" => ["myuser", "mydash", "widget-maps"],
                 "timeseries" => "last",
                 "transform" => nil
               },
               "data" => [
                 %{
                   "body" => %{
                     "action" => "sell",
                     "amount" => 3,
                     "datetime" => "\"2020-01-01T13:14:15Z\"",
                     "price" => 50
                   },
                   "datetime" => "2020-01-01T00:00:00Z"
                 }
               ]
             } == List.first(result)
    end
  end
end
