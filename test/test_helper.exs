ExUnit.configure(exclude: :million)
ExUnit.start()
Code.require_file("mock_data.exs", "test/")

if System.get_env("MIX_ENV") != "test_million" do
  Ecto.Adapters.SQL.Sandbox.mode(Bashboard.Repo, :manual)
end
