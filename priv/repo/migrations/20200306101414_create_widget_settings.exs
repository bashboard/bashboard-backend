defmodule Bashboard.Repo.Migrations.CreateWidgetSettings do
  use Ecto.Migration

  def change do
    create table(:widget_settings) do
      add(:user, :string, null: false)
      add(:dash, :string, null: false)
      add(:widget, :string, null: false)
      add(:body, :map)
      timestamps(type: :utc_datetime, updated_at: false)
    end

    create(index(:widget_settings, [:user, :dash, :widget]))
  end
end
