defmodule Bashboard.Repo.Migrations.CreateContinuousAggregation do
  use Ecto.Migration

  def change do
    for bucket <- ["minute", "hour", "day", "month"] do
      create_table_all(bucket)
      create_table_numbers(bucket)
      create_table_simple(bucket)
      create_table_maps(bucket)
    end
  end

  defp create_table_all(bucket) do
    name = "messages_all_#{bucket}"

    create table(name) do
      add(:user, :string, null: false)
      add(:dash, :string, null: false)
      add(:widget, :string, null: false)
      add(:count, :integer)
      add(:inserted_at, :utc_datetime)
    end

    create(index(name, [:inserted_at, :user, :dash, :widget]))
  end

  defp create_table_numbers(bucket) do
    name = "messages_numbers_#{bucket}"

    create table(name) do
      add(:user, :string, null: false)
      add(:dash, :string, null: false)
      add(:widget, :string, null: false)
      add(:avg, :float)
      add(:sum, :float)
      add(:inserted_at, :utc_datetime)
    end

    create(index(name, [:inserted_at, :user, :dash, :widget]))
  end

  defp create_table_simple(bucket) do
    name = "messages_simple_#{bucket}"

    create table(name) do
      add(:user, :string, null: false)
      add(:dash, :string, null: false)
      add(:widget, :string, null: false)
      add(:body, :map)
      add(:inserted_at, :utc_datetime)
    end

    create(index(name, [:inserted_at, :user, :dash, :widget]))
  end

  defp create_table_maps(bucket) do
    name = "messages_maps_#{bucket}"

    create table(name) do
      add(:user, :string, null: false)
      add(:dash, :string, null: false)
      add(:widget, :string, null: false)
      add(:body, :map)
      add(:inserted_at, :utc_datetime)
    end

    create(index(name, [:inserted_at, :user, :dash, :widget]))
  end
end
