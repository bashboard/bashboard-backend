defmodule Bashboard.Repo.Migrations.CreateUserReservations do
  use Ecto.Migration

  def change do
    create table(:user_reservations) do
      add(:user, :string, null: false)
      add(:email, :string, null: false)
      add(:updates_allowed, :boolean)
      add(:comments, :string)
      timestamps(type: :utc_datetime, updated_at: false)
    end

    create(index(:messages, [:user, :dash, :widget]))
  end
end
