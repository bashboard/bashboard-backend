defmodule Bashboard.MixProject do
  use Mix.Project

  def project do
    [
      app: :bashboard,
      version: "0.7.5",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:corsica, :logger, :plug_cowboy, :timex, :httpoison, :runtime_tools],
      mod: {Bashboard.Application, []}
    ]
  end

  defp aliases do
    [
      test: ["ecto.create", "ecto.migrate", "test"]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:poison, "~> 3.1"},
      {:plug_cowboy, "~> 2.0"},
      {:timex, "~> 3.6"},
      {:corsica, "~> 1.0"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:quantum, "~> 2.3"},
      {:httpoison, "~> 1.6"},
      {:decimal, "~> 1.0"},
      {:ecto_explain, "~> 0.1.2"},
      {:observer_cli, "~> 1.5"},
      {:jason, "~> 1.0"},
      {:pubsub, "~> 1.0"}
    ]
  end
end
