use Mix.Config

config :bashboard, Bashboard.Repo,
  database: "bashboard_test",
  username: "postgres",
  password: "postgres",
  hostname: "postgres",
  log: false,
  pool: Ecto.Adapters.SQL.Sandbox

config :bashboard, ecto_repos: [Bashboard.Repo]

config :bashboard, port: 4002
