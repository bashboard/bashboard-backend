use Mix.Config

config :bashboard, Bashboard.Repo,
  database: "bashboard_dev",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  log: false

config :bashboard, ecto_repos: [Bashboard.Repo]

config :bashboard, port: 4001

config :bashboard, Bashboard.Scheduler,
  debug_logging: false,
  jobs: [
    {{:extended, "*/30"}, {Bashboard.Dogfood, :system_info_running, []}},
    {"* * * * *", {Bashboard.Dogfood, :minute, []}},
    {"@reboot", {Bashboard.Dogfood, :system_info_once, []}},
    {"@reboot", {Bashboard.ExampleData, :populate, []}}
  ]

config :bashboard, :environment, :dev
