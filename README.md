[Bashboard](https://bashboard.io/) is an all in one dashboard that can receive data from any source online by POST requests (that includes webhooks btw). There are a ton of dashboar services out there and all of them praise their 1000 integrations to 3rd party services. Yet there are services still uncovered like Mailgun and Unbounce. Not exactly unpopular services either. So Bashboard aims to be a no-integartion dashboard instead.

The frontend and backend are separate. Perhaps people want the awesome timeseries aggregator only for their own app. There is no API documentation currently though, but issues and feature requests are taken seriously so you know what to do.

## Useful links

- [Website](https://bashboard.io/)
- [Frontend](https://gitlab.com/bashboard/bashboard-front-svelte) (Svelte)
- [Discord](https://discord.gg/zH37cu)
- [Reddit](https://www.reddit.com/r/bashboard/)

# Self-host Bashboard

## Prerequisites

- [Erlang](https://github.com/asdf-vm/asdf-erlang) v22
- [Elixir](https://github.com/asdf-vm/asdf-elixir) v1.9
- PostgreSQL (`docker run --name postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres:11`)

## Run dev server

```
mix deps.get
mix ecto.create && mix ecto.migrate
iex -S mix
```

Then check out http://localhost:4001/bashboard/system, you should see a blob of json there.

And now you can send data to your bashboard:

```bash
curl --data 1 http://localhost:4001/myuser/mydashboard/mywidget
curl --data 2 http://localhost:4001/myuser/mydashboard/mywidget
curl --data 3 http://localhost:4001/myuser/mydashboard/mywidget
curl --data 4 http://localhost:4001/myuser/mydashboard/mywidget
```

But it all looks better with [the front end](https://gitlab.com/bashboard/bashboard-front-svelte/).

## Testing

```
# normal
mix test

# Test performance with a million rows
MIX_ENV=test_million mix test --only million
```

## Deploy to production

Currently it's a mixture of the front and back ends.

- create server
- create user web
- `mkdir -p /var/lib/bashboard/back`
- `chown -R web:web /var/lib/bashboard`
- install node.js v13
- Enable systemd service
  - copy .service file from project root to server `/etc/systemd/system/bashboard-back.service`
  - `systemctl daemon-reload`
  - `systemctl start bashboard-back`
  - `systemctl enable bashboard-front`
- GitLab repo -> Settings -> CI / CD -> Variables
  - If you're going to deploy both front and back, it's easier to set these variables to group settings, not just repo.
  - `SSH_KNOWN_HOSTS` - this will be added to runner `~/.ssh/known_hosts`
  - `SSH_PRIVATE_KEY` - Generate a SSH key and put the private part here, public part to your server (both root and web users)
  - `SSH_SERVER_IP` - target server to connect to
  - `DB_NAME` - PostgreSQL connection param
  - `DB_USER` - PostgreSQL connection param
  - `DB_PASS` - PostgreSQL connection param
  - `DB_HOST` - PostgreSQL connection param
- run GitLab CD
