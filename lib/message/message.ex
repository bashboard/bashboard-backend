defmodule Bashboard.Message do
  use Ecto.Schema
  alias Bashboard.Repo, as: Repo
  alias Bashboard.Message, as: Message

  schema "messages" do
    field(:user, :string)
    field(:dash, :string)
    field(:widget, :string)
    field(:body, TypeJson)
    timestamps(type: :utc_datetime, updated_at: false)
  end

  def changeset(message, params) do
    message
    |> Ecto.Changeset.cast(params, [:user, :dash, :widget, :body])
    |> Ecto.Changeset.validate_required([:user, :dash, :widget])
  end

  @spec insert([bitstring], any) :: any
  def insert([user, dash, widget], body) do
    %Message{}
    |> changeset(%{user: user, dash: dash, widget: widget, body: body})
    |> Repo.insert()
  end
end
