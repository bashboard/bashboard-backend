defmodule Bashboard.MessageAggMap do
  use Ecto.Schema

  schema "messages_agg_map" do
    field(:user, :string)
    field(:dash, :string)
    field(:widget, :string)
    field(:body, TypeJson)
    field(:inserted_at, :utc_datetime)
  end
end
