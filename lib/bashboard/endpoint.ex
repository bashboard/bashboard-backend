defmodule Bashboard.Endpoint do
  @moduledoc """
  A Plug responsible for logging request info, parsing request body's as JSON,
  matching routes, and dispatching responses.
  """

  use Plug.Router
  use Plug.ErrorHandler

  plug(Corsica, origins: "*", log: [rejected: :warn, invalid: :debug, accepted: :debug])
  plug(:match)

  plug(Plug.Parsers,
    parsers: [:urlencoded, :json, WrongJson],
    json_decoder: Poison,
    pass: ["application/json", "application/x-www-form-urlencoded"]
  )

  plug(:dispatch)

  forward("/", to: Bashboard.Router)

  defp handle_errors(conn, %{kind: kind, reason: reason, stack: stack}) do
    IO.inspect(kind, label: :kind)
    IO.inspect(reason, label: :reason)
    IO.inspect(stack, label: :stack)
    send_resp(conn, conn.status, "Something went wrong")
  end
end
