defmodule Benchmark do
  use Agent

  # Things needed before package release
  #
  # FUNCTIONS
  # start
  # stop
  # get
  # puts
  # inspect
  # puts_stop
  # inspect_stop
  #
  # REMOVE
  # measure_function
  #
  # FEATURE
  # Don't print only total time, but also since last print

  def measure_function(function, args, label \\ "") do
    {duration, result} = :timer.tc(function, args)
    IO.inspect(duration / 1_000_000, label: "Benchmark #{label} (sec)")
    result
  end

  def start(name) when is_atom(name) do
    IO.puts("Benchmark start :#{name}")
    Agent.start_link(fn -> System.monotonic_time(:millisecond) end, name: name)
  end

  def stop(pid, name) when is_atom(name) do
    time = Agent.get(name, &(System.monotonic_time(:millisecond) - &1))
    Agent.stop(pid)
    IO.puts("Benchmark stop  :#{name} #{time}ms")

    {:ok, time}
  end

  def inspect(input, name, label \\ "") when is_atom(name) do
    time = Agent.get(name, &(System.monotonic_time(:millisecond) - &1))

    IO.puts("Benchmark time  #{time}ms #{label}")

    input
  end

  def puts(name, label \\ "") when is_atom(name) do
    time = Agent.get(name, &(System.monotonic_time(:millisecond) - &1))

    IO.puts("Benchmark time  #{time}ms #{label}")
  end
end
