defmodule Bashboard.PrintSQL do
  def inspect(query) do
    {query_string, values} = Ecto.Adapters.SQL.to_sql(:all, Bashboard.Repo, query)

    Enum.with_index(values, 1)
    |> Enum.reduce(query_string, fn {val, i}, acc -> String.replace(acc, "$#{i}", "'#{val}'") end)
    |> (&(&1 <> ";")).()
    |> IO.inspect(label: "SQL PRINT")

    query
  end
end
