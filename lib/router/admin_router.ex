defmodule Bashboard.Router.Admin do
  use Plug.Router
  alias Bashboard.Dash, as: Dash

  plug(:match)
  plug(:dispatch)

  get "/act" do
    PubSub.subscribers(:cagg)
    |> Enum.map(fn pid ->
      :sys.get_status(pid)
      |> elem(3)
      |> List.last()
      |> elem(1)
      |> Map.get(:namespace)
      |> (&"/#{elem(&1, 0)}/#{elem(&1, 1)}").()
    end)
    |> Enum.uniq()
    |> Enum.sort()
    |> Poison.encode!(pretty: true)
    |> (&send_resp(conn, 200, &1)).()
  end

  get "/all" do
    Dash.get_unique_namespaces()
    |> Poison.encode!(pretty: true)
    |> (&send_resp(conn, 200, &1)).()
  end
end
