defmodule Bashboard.Router.WidgetSocket do
  alias Bashboard.Widget, as: Widget
  @behaviour :cowboy_websocket

  def init(req, _state) do
    params = URI.decode_query(req.qs)

    %{user: user, dash: dash, widget: widget} = req.bindings

    state = %{
      last_date: Timex.parse!(params["datetime"], "{ISO:Extended}"),
      namespace: {user, dash, widget},
      params: params
    }

    {:cowboy_websocket, req, state, %{idle_timeout: 10 * 60 * 1000}}
  end

  def websocket_init(state) do
    PubSub.subscribe(self(), :cagg)
    {:ok, state}
  end

  def websocket_handle({:text, message}, state) do
    json = Poison.decode!(message)
    websocket_handle({:json, json}, state)
  end

  def websocket_handle({:json, _json}, state) do
    {:reply, {:text, "Websocket doesn't accept data."}, state}
  end

  def websocket_info({:cagg, :done}, state) do
    widget = Widget.get(state.namespace, state.params, state.last_date)
    json = Poison.encode!(widget)

    state =
      Map.update!(state, :last_date, fn _x ->
        widget.data |> List.last() |> Map.get(:datetime)
      end)

    {:reply, {:text, json}, state}
  end

  def websocket_info(_info, state) do
    {:ok, state}
  end

  def terminate(reason, _req, _state) do
    IO.inspect(reason, label: "reason")
    :ok
  end
end
