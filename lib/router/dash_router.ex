defmodule Bashboard.Router.Dash do
  use Plug.Router
  alias Bashboard.Dash, as: Dash

  plug(:match)
  plug(:dispatch)

  get "/:user/:dash/cou" do
    conn.path_info
    |> Enum.take(2)
    |> Dash.get_count()
    |> Poison.encode!()
    |> (&send_resp(conn, 200, &1)).()
  end

  get "/:user/:dash" do
    Dash.get(conn.path_info)
    |> Poison.encode!()
    |> (&send_resp(conn, 200, &1)).()
  end
end
