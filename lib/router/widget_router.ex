defmodule Bashboard.Router.Widget do
  use Plug.Router
  alias Bashboard.Widget, as: Widget
  alias Bashboard.Message, as: Message
  alias Bashboard.WidgetSettings, as: WidgetSettings

  plug(:match)
  plug(:dispatch)

  get "/:user/:dash/:widget" do
    datetime =
      case get_in(conn.query_params, ["datetime"]) do
        nil -> Timex.to_datetime({{1980, 1, 1}, {0, 0, 0}}, "UTC")
        val -> Timex.parse!(val, "{ISO:Extended}")
      end

    # Browser can convert spaces to %20, let's convert them back.
    # Of course now the widget can't contain %20.. dunno what's best.
    conn.path_info
    |> Enum.map(fn x -> String.replace(x, "%20", " ", global: true) end)
    |> List.to_tuple()
    |> Widget.get(conn.query_params, datetime)
    |> Poison.encode!()
    |> (&send_resp(conn, 200, &1)).()
  end

  get "/:user/:dash/:widget/settings" do
    conn.path_info
    |> Enum.take(3)
    |> Enum.map(fn x -> String.replace(x, "%20", " ", global: true) end)
    |> List.to_tuple()
    |> WidgetSettings.get()
    |> Poison.encode!()
    |> (&send_resp(conn, 200, &1)).()
  end

  post "/:user/:dash/:widget/settings" do
    namespace =
      conn.path_info
      |> Enum.take(3)
      |> Enum.map(fn x -> String.replace(x, "%20", " ", global: true) end)
      |> List.to_tuple()

    Map.get_lazy(conn.body_params, "_json", fn -> conn.body_params end)
    |> Bashboard.Options.atomize_map()
    |> Map.new()
    |> (&WidgetSettings.insert(namespace, &1)).()
    |> case do
      {:ok, _widget_settings} -> send_resp(conn, 200, "success")
      {:error, msg} -> raise msg
    end
  end

  post "/:user/:dash/:widget" do
    data = Map.get_lazy(conn.body_params, "_json", fn -> conn.body_params end)

    # No content-type means need to parse manually
    data =
      if length(conn.req_headers) == 0 do
        case Plug.Conn.read_body(conn) do
          {:ok, result, _conn} ->
            case Float.parse(result) do
              :error -> result
              {f, _} -> f
            end
        end
      else
        data
      end

    data =
      if is_map(data) do
        # `curl -d 12 url` creates body %{"12" => nil}. Convert this back to number,
        case Map.to_list(data) do
          [{single_value, nil}] ->
            case Float.parse(single_value) do
              :error -> single_value
              {result, _} -> result
            end

          # POST request, but body is empty
          [] ->
            nil

          multi_value ->
            Map.new(multi_value)
        end
      else
        data
      end

    conn.path_info
    |> Enum.map(fn x -> String.replace(x, "%20", " ", global: true) end)
    |> Message.insert(data)

    send_resp(conn, 200, "success")
  end
end
