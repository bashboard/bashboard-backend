defmodule Bashboard.Widget do
  import Ecto.Query
  import Bashboard.Helpers
  alias Bashboard.Repo, as: Repo
  alias Bashboard.Message, as: Message
  alias Bashboard.WidgetSettings, as: WidgetSettings
  alias Bashboard.Options, as: Options
  require Bashboard.Dogfood
  require Logger

  @max_data_length 100

  @spec get({bitstring, bitstring, bitstring}, list) :: map
  @spec get({bitstring, bitstring, bitstring}, map) :: map
  def get(
        namespace,
        options \\ [],
        datetime \\ Timex.to_datetime({{1980, 1, 1}, {0, 0, 0}}, "UTC")
      )

  def get(namespace, options, datetime) when is_map(options) do
    options
    |> Options.atomize_map()
    |> (&get(namespace, &1, datetime)).()
  end

  def get(namespace, options, datetime) when is_list(options) do
    Bashboard.Dogfood.report_duration "Widget request duration", namespace do
      case options do
        [:raw | _] ->
          {:raw, nil, get_raw(namespace)}

        [:all_confs | _] ->
          {:all_confs, nil, get_all_possible_configurations(namespace)}

        [{:timeseries, "last"} | _] ->
          {:last, nil, get_last(namespace)}

        [{:timeseries, "series"} | _] ->
          {:series, nil, get_series(namespace, datetime)}

        [timeseries: timeseries, transform: transform] ->
          {timeseries, transform, get_timeseries(namespace, {timeseries, transform}, datetime)}

        [timeseries: timeseries] ->
          {timeseries, :count_messages,
           get_timeseries(namespace, {timeseries, :count_messages}, datetime)}

        [transform: transform] ->
          {"minute", transform, get_timeseries(namespace, {"minute", transform}, datetime)}

        [] ->
          get_saved_configuration(namespace)
          |> case do
            :default_options ->
              get(namespace, %{"timeseries" => "last"})
              |> put_in([:meta, :no_settings], true)

            settings ->
              get(namespace, settings)
          end
      end
      |> case do
        {:all_confs, nil, results} ->
          results

        {ts, tr, []} ->
          %{
            data: [],
            meta: %{
              namespace: Tuple.to_list(namespace),
              timeseries: ts,
              transform: tr
            }
          }

        {ts, tr, results} ->
          %{
            data: results |> fill_date_blanks(ts) |> format_widget_results(),
            meta: %{
              namespace: Tuple.to_list(namespace),
              timeseries: ts,
              transform: tr
            }
          }

        %{meta: _meta} = already_has_extra ->
          already_has_extra
      end
    end
  end

  @spec get_raw({bitstring, bitstring, bitstring}) :: map
  defp get_raw({user, dash, widget}) do
    from(m in Message,
      where: m.user == ^user and m.dash == ^dash and m.widget == ^widget,
      order_by: m.inserted_at,
      limit: @max_data_length,
      select: {m.inserted_at, m.body}
    )
    |> Repo.all()
  end

  @spec get_saved_configuration({bitstring, bitstring, bitstring}) :: any
  defp get_saved_configuration(namespace) do
    WidgetSettings.get(namespace)
    |> case do
      %WidgetSettings{body: body} when is_map(body) and map_size(body) > 0 -> body
      _ -> :default_options
    end
  end

  @spec get_all_possible_configurations({bitstring, bitstring, bitstring}) :: map
  defp get_all_possible_configurations(namespace) do
    Options.get_all_combinations()
    |> Enum.map(fn {ts, tf} ->
      get(namespace, timeseries: ts, transform: tf)
    end)
    |> Enum.filter(fn widget ->
      widget
      |> get_in([:data])
      |> length()
      |> (&(&1 > 0)).()
    end)
  end

  @spec get_last({bitstring, bitstring, bitstring}) :: list
  defp get_last({user, dash, widget}) do
    from(m in Message,
      where: m.user == ^user and m.dash == ^dash and m.widget == ^widget,
      order_by: m.inserted_at,
      limit: 1,
      select: {m.inserted_at, m.body}
    )
    |> last
    |> Repo.all()
  end

  @spec get_series({bitstring, bitstring, bitstring}, DateTime.t()) :: [map]
  defp get_series({user, dash, widget}, datetime) do
    from(m in Message,
      where:
        m.user == ^user and m.dash == ^dash and m.widget == ^widget and m.inserted_at >= ^datetime,
      order_by: [desc: m.inserted_at],
      limit: @max_data_length,
      select: {m.inserted_at, m.body}
    )
    |> Repo.all()
    |> Enum.reverse()
  end

  @spec get_timeseries({bitstring, bitstring, bitstring}, {bitstring, atom}, DateTime.t()) ::
          list
  defp get_timeseries(namespace, options, datetime) do
    case options do
      {timeseries, :avg_numbers} ->
        avg_numbers_query(namespace, timeseries, datetime)

      {timeseries, :sum_numbers} ->
        sum_numbers_query(namespace, timeseries, datetime)

      {timeseries, :sum_map} ->
        sum_map_query(namespace, timeseries, datetime)

      {timeseries, :avg_map} ->
        avg_map_query(namespace, timeseries, datetime)

      {timeseries, :count_messages} ->
        count_messages_query(namespace, timeseries, datetime)

      {timeseries, :categorize_strings} ->
        categorize_strings_query(namespace, timeseries, datetime)

      {timeseries, nil} ->
        count_messages_query(namespace, timeseries, datetime)
    end
    |> decimals_to_float()
  end

  defp decimals_to_float(data) do
    Enum.map(data, fn {datetime, val} ->
      case val do
        x when is_list(x) -> {datetime, Enum.map(x, fn no -> decimal_to_float(no) end)}
        x -> {datetime, decimal_to_float(x)}
      end
    end)
  end

  defp decimal_to_float(val) do
    case Decimal.decimal?(val) do
      true -> Decimal.to_float(val)
      false -> val
    end
  end

  defp avg_numbers_query({user, dash, widget}, timeseries, datetime) do
    from(m in "messages_numbers_#{timeseries}",
      where:
        m.user == ^user and m.dash == ^dash and m.widget == ^widget and m.inserted_at >= ^datetime,
      limit: @max_data_length,
      order_by: [desc: m.inserted_at],
      select: {m.inserted_at, m.avg}
    )
    |> Repo.all()
    |> Enum.reverse()
  end

  defp sum_numbers_query({user, dash, widget}, timeseries, datetime) do
    from(m in "messages_numbers_#{timeseries}",
      where:
        m.user == ^user and m.dash == ^dash and m.widget == ^widget and m.inserted_at >= ^datetime,
      limit: @max_data_length,
      order_by: [desc: m.inserted_at],
      select: {m.inserted_at, m.sum}
    )
    |> Repo.all()
    |> Enum.reverse()
  end

  defp sum_map_query({user, dash, widget}, timeseries, datetime) do
    from(m in {"messages_maps_#{timeseries}", Bashboard.MessageAggMap},
      where:
        m.user == ^user and m.dash == ^dash and m.widget == ^widget and m.inserted_at >= ^datetime,
      cross_join: fragment("jsonb_each(?)", m.body),
      limit: @max_data_length,
      order_by: [desc: m.inserted_at],
      select: {m.inserted_at, fragment("json_build_object(key, (value->>0)::float) AS body")}
    )
    |> Repo.all()
    |> Enum.reverse()
    |> Enum.reduce([], fn
      item, [] ->
        [item]

      {inserted_at, body}, acc ->
        {last_date, last_body} = List.last(acc)

        if Timex.compare(last_date, inserted_at) === 0 do
          List.delete_at(acc, -1) ++ [{inserted_at, Map.merge(last_body, body)}]
        else
          acc ++ [{inserted_at, body}]
        end
    end)
  end

  defp avg_map_query({user, dash, widget}, timeseries, datetime) do
    from(m in {"messages_maps_#{timeseries}", Bashboard.MessageAggMap},
      where:
        m.user == ^user and m.dash == ^dash and m.widget == ^widget and m.inserted_at >= ^datetime,
      cross_join: fragment("jsonb_each(?)", m.body),
      limit: @max_data_length,
      order_by: [desc: m.inserted_at],
      select: {m.inserted_at, fragment("json_build_object(key, (value->>1)::float) AS body")}
    )
    |> Repo.all()
    |> Enum.reverse()
    |> Enum.reduce([], fn
      item, [] ->
        [item]

      {inserted_at, body}, acc ->
        {last_date, last_body} = List.last(acc)

        if Timex.compare(last_date, inserted_at) === 0 do
          List.delete_at(acc, -1) ++ [{inserted_at, Map.merge(last_body, body)}]
        else
          acc ++ [{inserted_at, body}]
        end
    end)
  end

  defp count_messages_query({user, dash, widget}, timeseries, datetime) do
    from(m in {"messages_all_#{timeseries}", Bashboard.MessageAggAll},
      where:
        m.user == ^user and m.dash == ^dash and m.widget == ^widget and m.inserted_at >= ^datetime,
      limit: @max_data_length,
      order_by: [desc: m.inserted_at],
      select: {m.inserted_at, m.count}
    )
    |> Repo.all()
    |> Enum.reverse()
  end

  defp categorize_strings_query({user, dash, widget}, timeseries, datetime) do
    from(m in {"messages_strings_#{timeseries}", Bashboard.MessageAggString},
      where:
        m.user == ^user and m.dash == ^dash and m.widget == ^widget and m.inserted_at >= ^datetime,
      limit: @max_data_length,
      order_by: [desc: m.inserted_at],
      select: {m.inserted_at, m.body}
    )
    |> Repo.all()
    |> Enum.reverse()
  end

  defp fill_date_blanks(widgets, bucket) when length(widgets) == 1 or is_atom(bucket) do
    widgets
  end

  defp fill_date_blanks(widgets, bucket) do
    {first_dt, _} = List.first(widgets)
    {last_dt, _} = List.last(widgets)
    step = bucket_to_timex(bucket)

    target_length = min(@max_data_length, Timex.diff(last_dt, first_dt, step))

    0..target_length
    |> Enum.map(fn index ->
      dt = Timex.shift(last_dt, [{step, -1 * index}])
      Enum.find(widgets, {dt, nil}, fn {datetime, _body} -> dt == datetime end)
    end)
    |> Enum.reverse()
  end

  defp format_widget_results(rows) do
    for {datetime, body} <- rows do
      %{
        datetime: datetime,
        body: body
      }
    end
  end
end
