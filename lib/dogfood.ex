defmodule Bashboard.Dogfood do
  alias Bashboard.Message, as: Message
  alias Bashboard.UserReservartion, as: UserReservartion
  alias Bashboard.Repo, as: Repo
  import Ecto.Query

  @spec system_info_running :: any
  def system_info_running do
    (:erlang.memory()[:total] / 1_000_000)
    |> Float.round(2)
    |> send_msg("Memory-usage-(mb)")

    :erlang.statistics(:wall_clock)
    |> elem(0)
    |> (&(&1 / (1000 * 60 * 60))).()
    |> Float.round(2)
    |> send_msg("Uptime-(hours)")

    PubSub.subscribers(:cagg)
    |> Enum.map(fn pid ->
      :sys.get_status(pid)
      |> elem(3)
      |> List.last()
      |> elem(1)
      |> Map.get(:namespace)
      |> elem(0)
    end)
    |> Enum.uniq()
    |> length
    |> send_msg("Active-Users", "landing")
  end

  def minute do
    from(m in UserReservartion)
    |> Repo.all()
    |> length
    |> send_msg("Users-Reserved", "landing")
  end

  @spec system_info_once :: any
  def system_info_once do
    Application.spec(:bashboard, :vsn)
    |> to_string()
    |> send_msg("Bashboard-version")

    System.version()
    |> send_msg("Elixir-version")
  end

  @spec send_msg(any, bitstring, bitstring) :: any
  def send_msg(data, widget, dash \\ "system") do
    Message.insert(["bashboard", dash, widget], data)
  end

  defmacro report_duration(name, namespace, units \\ :millisecond, do: yield) do
    quote do
      start = System.monotonic_time(unquote(units))
      result = unquote(yield)
      time_spent = System.monotonic_time(unquote(units)) - start

      name = String.replace(unquote(name), " ", "+")

      # Send the log (but don't log itself)
      case unquote(namespace) do
        {"bashboard", "system", ^name} -> nil
        _ -> Task.async(fn -> Bashboard.Dogfood.send_msg(time_spent, unquote(name)) end)
      end

      result
    end
  end
end
