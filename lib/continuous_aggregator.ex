defmodule Bashboard.ContinuousAggregator do
  import Ecto.Query
  import Bashboard.Helpers
  alias Bashboard.Repo, as: Repo
  alias Bashboard.Message, as: Message
  alias Bashboard.MessageAggMap, as: MessageAggMap
  alias Bashboard.MessageAggNumber, as: MessageAggNumber
  alias Bashboard.MessageAggString, as: MessageAggString

  @transforms [
    "all",
    "numbers",
    "strings",
    "maps"
  ]

  @buckets [
    "minute",
    "hour",
    "day",
    "month"
  ]

  def all_once() do
    {:ok, bm_pid} = Benchmark.start(:cagg)

    for transform <- @transforms,
        {source_table, bucket} <-
          @buckets
          |> Enum.drop(-1)
          |> Enum.map(&"messages_#{transform}_#{&1}")
          |> (&[Message | &1]).()
          |> Enum.zip(@buckets) do
      update_query(transform, bucket, source_table)
    end

    {:ok, time} = Benchmark.stop(bm_pid, :cagg)

    if !Bashboard.Helpers.testing_mode?() do
      Bashboard.Dogfood.send_msg(time, "Continuous Aggregation Batch Duration (ms)")
    end

    PubSub.publish(:cagg, {:cagg, :done})
  end

  def get_combinations() do
    for t <- @transforms,
        b <- @buckets do
      "messages_#{t}_#{b}"
    end
  end

  defp update_query(transform, bucket, source_table) do
    final_date = get_last_date(source_table, bucket, -4)

    case transform do
      "all" -> update_all_query(bucket, source_table, final_date)
      "numbers" -> update_numbers_query(bucket, source_table, final_date)
      "strings" -> update_strings_query(bucket, source_table, final_date)
      "maps" -> update_maps_query(bucket, source_table, final_date)
    end
  end

  defp update_all_query(bucket, source_table, final_date, recursion_count \\ 0)

  defp update_all_query(_, _, _, 100) do
    :ok_recursion_limit_reached
  end

  defp update_all_query(bucket, source_table, final_date, recursion_count) do
    target_table = "messages_all_#{bucket}"

    last_date = get_last_date(target_table, bucket)

    pre_filter = get_pre_filter_query(nil, last_date, source_table, Bashboard.MessageAggAll)

    source_materials =
      from(m in subquery(pre_filter),
        group_by: [m.user, m.dash, m.widget, fragment("datetime")],
        select: %{
          user: m.user,
          dash: m.dash,
          widget: m.widget,
          datetime: fragment("date_trunc(?, ?) as datetime", ^bucket, m.inserted_at)
        }
      )
      |> (&(case source_table do
              Message -> select_merge(&1, [m], %{count: count(m)})
              _ -> select_merge(&1, [m], %{count: sum(m.count)})
            end)).()
      |> Repo.all()
      |> move_datetime_to_inserted_at()
      |> to_utc_datetime()
      |> delete_duplicates(target_table)

    Repo.insert_all({target_table, Bashboard.MessageAggAll}, source_materials)

    with %{inserted_at: last_inserted_at} <- List.last(source_materials),
         true <- Timex.before?(last_inserted_at, final_date) do
      update_all_query(bucket, source_table, final_date, recursion_count + 1)
    else
      _ ->
        :ok
    end
  end

  defp update_numbers_query(bucket, source_table, final_date, recursion_count \\ 0)

  defp update_numbers_query(_, _, _, 100) do
    :ok_recursion_limit_reached
  end

  defp update_numbers_query(bucket, source_table, final_date, recursion_count) do
    target_table = "messages_numbers_#{bucket}"

    last_date = get_last_date(target_table, bucket)

    pre_filter =
      get_pre_filter_query(
        source_table == Message,
        last_date,
        source_table,
        Bashboard.MessageAggNumber
      )

    source_materials =
      from(m in subquery(pre_filter),
        group_by: [m.user, m.dash, m.widget, fragment("datetime")],
        select: %{
          user: m.user(),
          dash: m.dash(),
          widget: m.widget(),
          datetime: fragment("date_trunc(?, ?) AS datetime", ^bucket, m.inserted_at)
        }
      )
      |> (&(case source_table do
              Message ->
                select_merge(&1, [m], %{
                  avg: fragment("AVG(?::numeric)::float", m.body),
                  sum: fragment("SUM(?::numeric)::float", m.body)
                })

              _ ->
                select_merge(&1, [m], %{
                  avg: fragment("AVG(?::numeric)::float", m.avg),
                  sum: fragment("SUM(?::numeric)::float", m.sum)
                })
            end)).()
      |> Repo.all()
      |> move_datetime_to_inserted_at()
      |> to_utc_datetime()
      |> delete_duplicates(target_table)

    Repo.insert_all({target_table, Bashboard.MessageAggNumber}, source_materials)

    with %{inserted_at: last_inserted_at} <- List.last(source_materials),
         true <- Timex.before?(last_inserted_at, final_date) do
      update_numbers_query(bucket, source_table, final_date, recursion_count + 1)
    else
      _ ->
        :ok
    end
  end

  defp update_strings_query(bucket, source_table, final_date, recursion_count \\ 0)

  defp update_strings_query(_, _, _, 100) do
    :ok_recursion_limit_reached
  end

  defp update_strings_query(bucket, Message, final_date, recursion_count) do
    target_table = "messages_strings_#{bucket}"
    last_date = get_last_date(target_table, bucket)
    pre_filter = get_pre_filter_query(true, last_date, Message, Bashboard.MessageAggString)

    source_materials =
      from(m in subquery(pre_filter),
        group_by: [m.user, m.dash, m.widget, fragment("datetime"), m.body],
        select: %{
          user: m.user,
          dash: m.dash,
          widget: m.widget,
          datetime: fragment("date_trunc(?, ?) AS datetime", ^bucket, m.inserted_at),
          body:
            fragment(
              "json_build_object(trim('\"' FROM ?::text), COUNT(?))",
              m.body,
              m.body
            )
        }
      )
      |> Repo.all()
      |> move_datetime_to_inserted_at()
      |> group_rows_by_datetime()
      |> Map.values()
      |> to_utc_datetime()
      |> delete_duplicates(target_table)

    Repo.insert_all({target_table, Bashboard.MessageAggString}, source_materials)

    with %{inserted_at: last_inserted_at} <- List.last(source_materials),
         true <- Timex.before?(last_inserted_at, final_date) do
      update_strings_query(bucket, Message, final_date, recursion_count + 1)
    else
      _ ->
        :ok
    end
  end

  defp update_strings_query(bucket, source_table, final_date, recursion_count) do
    target_table = "messages_strings_#{bucket}"
    last_date = get_last_date(target_table, bucket)
    pre_filter = get_pre_filter_query(false, last_date, source_table, Bashboard.MessageAggString)

    source_materials =
      from(m in subquery(pre_filter),
        cross_join: fragment("jsonb_each(?)", m.body),
        group_by: [m.user, m.dash, m.widget, fragment("datetime"), fragment("key")],
        select: %{
          user: m.user,
          dash: m.dash,
          widget: m.widget,
          datetime: fragment("date_trunc(?, ?) AS datetime", ^bucket, m.inserted_at),
          body: fragment("json_build_object(key, SUM(value::numeric))")
        }
      )
      |> Repo.all()
      |> move_datetime_to_inserted_at()
      |> group_rows_by_datetime()
      |> Map.values()
      |> to_utc_datetime()
      |> delete_duplicates(target_table)

    Repo.insert_all({target_table, Bashboard.MessageAggString}, source_materials)

    with %{inserted_at: last_inserted_at} <- List.last(source_materials),
         true <- Timex.before?(last_inserted_at, final_date) do
      update_strings_query(bucket, source_table, final_date, recursion_count + 1)
    else
      _ ->
        :ok
    end
  end

  defp update_maps_query(bucket, source_table, final_date, recursion_count \\ 0)

  defp update_maps_query(_, _, _, 100) do
    :ok_recursion_limit_reached
  end

  defp update_maps_query(bucket, source_table, final_date, recursion_count) do
    target_table = "messages_maps_#{bucket}"

    last_date = get_last_date(target_table, bucket)

    pre_filter =
      get_pre_filter_query(
        source_table == Message,
        last_date,
        source_table,
        Bashboard.MessageAggMap
      )

    where =
      case source_table do
        Message ->
          dynamic([m], fragment("jsonb_typeof(value)") == "number")

        _ ->
          true
      end

    source_materials =
      from(m in subquery(pre_filter),
        cross_join: fragment("jsonb_each(?)", m.body),
        where: ^where,
        group_by: [m.user, m.dash, m.widget, fragment("datetime"), fragment("key")],
        select: %{
          user: m.user,
          dash: m.dash,
          widget: m.widget,
          datetime: fragment("date_trunc(?, ?) AS datetime", ^bucket, m.inserted_at)
        }
      )
      |> (&(case source_table do
              Message ->
                select_merge(&1, [m], %{
                  body:
                    fragment(
                      "json_build_object(key, json_build_array(SUM(value::numeric), AVG(value::numeric)))"
                    )
                })

              _ ->
                select_merge(&1, [m], %{
                  body:
                    fragment(
                      "json_build_object(key, json_build_array(SUM((value->>0)::numeric), AVG((value->>1)::numeric))) AS body"
                    )
                })
            end)).()
      |> Repo.all()
      |> move_datetime_to_inserted_at()
      |> group_rows_by_datetime()
      |> Map.values()
      |> to_utc_datetime()
      |> delete_duplicates(target_table)

    Repo.insert_all({target_table, Bashboard.MessageAggMap}, source_materials)

    with %{inserted_at: last_inserted_at} <- List.last(source_materials),
         true <- Timex.before?(last_inserted_at, final_date) do
      update_maps_query(bucket, source_table, final_date, recursion_count + 1)
    else
      _ ->
        :ok
    end
  end

  defp get_last_date(target_table, bucket, buffer_amount \\ -2) do
    from(m in target_table,
      order_by: m.inserted_at,
      select: m.inserted_at
    )
    |> last
    |> Repo.one()
    |> case do
      nil -> nil
      date -> Timex.shift(date, [{bucket_to_timex(bucket), buffer_amount}])
    end
  end

  defp to_utc_datetime(rows) when is_list(rows) do
    Enum.map(rows, fn row -> Map.update!(row, :inserted_at, &to_utc_datetime(&1)) end)
  end

  defp to_utc_datetime(datetime) do
    Ecto.Type.cast(:utc_datetime, datetime)
    |> case do
      {:ok, date} -> date
      _ -> nil
    end
  end

  defp delete_duplicates(source_results, target_table) do
    Repo.delete_all(
      from(m in target_table,
        where: m.inserted_at in ^Enum.map(source_results, fn row -> row.inserted_at end)
      )
    )

    source_results
  end

  defp get_pre_filter_query(generate_typeof, last_date, source_table, schema) do
    where =
      case last_date do
        nil -> true
        _ -> dynamic([m], m.inserted_at > ^last_date)
      end

    where =
      if source_table == Message do
        case schema do
          MessageAggMap -> dynamic([m], fragment("jsonb_typeof(body)") == "object" and ^where)
          MessageAggNumber -> dynamic([m], fragment("jsonb_typeof(body)") == "number" and ^where)
          MessageAggString -> dynamic([m], fragment("jsonb_typeof(body)") == "string" and ^where)
          _ -> where
        end
      else
        where
      end

    select_map =
      case source_table do
        Message -> Message
        _ -> schema
      end
      |> (& &1.__struct__()).()
      |> Map.keys()
      |> Enum.filter(fn x -> !String.starts_with?(Atom.to_string(x), "_") end)

    from(m in source_table,
      where: ^where,
      limit: 10000,
      order_by: m.inserted_at,
      select: map(m, ^select_map)
    )
    |> (&(case generate_typeof do
            true -> select_merge(&1, [m], %{typeof: fragment("jsonb_typeof(?)", m.body)})
            _ -> &1
          end)).()
  end

  defp group_rows_by_datetime(rows) do
    rows
    |> Enum.reduce(%{}, fn row, acc ->
      Map.update(
        acc,
        to_string(row.inserted_at) <> row.user <> row.dash <> row.widget,
        row,
        fn %{body: acc_body} ->
          Map.update(row, :body, row.body, fn new_body ->
            Map.merge(acc_body, new_body, fn _key, v1, v2 ->
              case {v1, v2} do
                # sum/avg already aggregated
                {[sum1, avg1], [sum2, avg2]} -> [(sum1 + sum2) / 2, avg1 + avg2]
                # body is number
                {no1, no2} when is_number(no1) and is_number(no2) -> no1 + no2
              end
            end)
          end)
        end
      )
    end)
  end

  defp move_datetime_to_inserted_at(rows) do
    rows
    |> Enum.map(fn row ->
      Map.put(row, :inserted_at, row.datetime)
      |> Map.delete(:datetime)
    end)
  end
end
