defmodule Bashboard.Dash do
  import Ecto.Query
  alias Bashboard.Repo, as: Repo
  alias Bashboard.Widget, as: Widget

  @spec get([bitstring]) :: map
  def get([user, dash]) do
    from(m in "messages_all_month",
      where: m.user == ^user and m.dash == ^dash,
      group_by: m.widget,
      select: m.widget
    )
    |> Repo.all()
    |> Map.new(fn widget -> {widget, Widget.get({user, dash, widget})} end)
  end

  def get_unique_namespaces() do
    from(m in "messages_all_month",
      distinct: [m.user, m.dash, m.widget],
      select: {m.user, m.dash, m.widget}
    )
    |> Repo.all()
    |> Enum.map(fn {user, dash, widget} -> "/#{user}/#{dash}/#{widget}" end)
  end

  def get_count([user, dash]) do
    messages_count =
      from(m in "messages",
        where: m.user == ^user and m.dash == ^dash,
        select: count(m)
      )
      |> Repo.one()

    month_count =
      from(m in "messages_all_month",
        where: m.user == ^user and m.dash == ^dash,
        select: count(m)
      )
      |> Repo.one()

    %{
      messages_count: messages_count,
      month_count: month_count
    }
  end
end
