defmodule Bashboard.ExampleData do
  import Ecto.Query
  alias Bashboard.Repo, as: Repo
  alias Bashboard.Message, as: Message

  @user "examples"

  def populate() do
    delete_old()

    data = gen_data()

    [
      last: data,
      series: [
        numbers: data.numbers,
        maps: data.maps
      ],
      timeseries: [
        {"numbers-sum-minute", data.numbers},
        {"maps-sum-hour", data.maps},
        {"strings-sum-month", data.numbers}
      ],
      avg: [
        {"numbers-minute", data.numbers},
        {"maps-minute", data.maps}
      ],
      sum: [
        {"numbers-minute", data.numbers},
        {"maps-minute", data.maps}
      ],
      categorize: [
        {"strings-minute", data.strings}
      ],
      count: [
        {"numbers-minute", data.numbers},
        {"strings-hour", data.strings},
        {"maps-day", data.maps}
      ]
    ]
    |> save_batch()
  end

  defp save_batch(agg_list, interval \\ 30) do
    for {agg, type_list} <- agg_list,
        {type, body_list} <- type_list,
        body <- body_list do
      [
        @user,
        Atom.to_string(agg),
        if(is_atom(type), do: Atom.to_string(type), else: type)
      ]
      |> Message.insert(body)

      :timer.sleep(interval * 1000)
    end
  end

  defp delete_old do
    for table <- Bashboard.ContinuousAggregator.get_combinations() ++ [Message] do
      from(m in table,
        where: m.user == ^@user
      )
      |> Repo.delete_all()
    end
  end

  defp gen_data do
    %{
      numbers: Enum.map(1..30, fn _ -> :rand.uniform(10) end),
      strings: ["home", "examples", "price", "price", "home", "price"],
      maps:
        Enum.map(1..15, fn _ ->
          %{price: Enum.random(1..10), amount: Enum.random(1..10), age: Enum.random(18..30)}
        end)
    }
  end
end
